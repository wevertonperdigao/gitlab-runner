#!/bin/sh

URL_GITLAB=http://172.100.10.24/

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token PRsHDNcUiFnhkTUbhgLc \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-SERVICE-REGISTER' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token xkym_W4nWiJz5JtrHbzW \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-PROJETO' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token JA6eqhtrjpB1nCCEuTBg \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-GATEWAY' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token J-JK3LoXWcgTitfZtLjy \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-AUTHENTICATION' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token xh7p4So5EqZ3dcwJHhGC \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-STORAGE' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token iLu72yGW22n5YinpuWfu \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-CENTER-SWAGGER' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token sTZHwrLkFxUomeiT82uK \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-ADMIN' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token 8dHVzxSnYD1toXoR4CRb \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-CONFIG' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token Lfy6xjY3z2s1876n3eif \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-MAIL-SENDER' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token dc7oYVpUnuM_sSPfxeBW \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-SGP-FRONTEND' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token RpcdyaAwN7v42fQFWznm \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-HIRO-FRONTEND' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network

docker exec -it gitlab-runner \
  gitlab-runner register \
  --non-interactive \
  --registration-token zs8FkRzh3yTuBXj8QjX7 \
  --locked=false \
  --url ${URL_GITLAB} \
  --executor docker \
  --description 'RUNNER-CONTROLE-ACESSO-FRONTEND' \
  --docker-image docker:stable \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-network-mode gitlab-network
